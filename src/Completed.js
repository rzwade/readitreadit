import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from './UserContext';
import './Completed.css';

const Completed = () => {
  const { userInfo } = useContext(UserContext);
  const [completedItems, setCompletedItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [review, setReview] = useState('');
  const [rating, setRating] = useState(0);
  const [selectedBookId, setSelectedBookId] = useState(null);
  const [author, setAuthor] = useState('');
  const [title, setTitle] = useState('');

  useEffect(() => {
    const fetchCompletedItems = async () => {
      try {
        if (userInfo?.id) {
          const response = await fetch(`https://readitreadit.onrender.com/api/completed/${userInfo.id}`, {
            credentials: 'include',
          });
          if (response.ok) {
            const items = await response.json();
            setCompletedItems(items);
          } else {
            setError(new Error('Failed to fetch completed items'));
          }
        }
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchCompletedItems();
  }, [userInfo]);

  const handleRemoveFromCompleted = async (itemId) => {
    try {
      const response = await fetch(`https://readitreadit.onrender.com/api/completed/${itemId}`, {
        method: 'DELETE',
        credentials: 'include',
      });
      if (response.ok) {
        setCompletedItems(completedItems.filter(item => item._id !== itemId));
      } else {
        setError(new Error('Failed to remove item from completed'));
      }
    } catch (error) {
      setError(error);
    }
  };

  const handleModalOpen = (itemId, bookId, author, title) => {
    setSelectedBookId(bookId);
    setAuthor(author);
    setTitle(title);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      console.log("Submitting review...");
      const response = await fetch('https://readitreadit.onrender.com/api/reviews', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({ reviewText: review, rating, userId: userInfo.id, bookId: selectedBookId, author, title }),
      });

      if (response.ok) {
        setIsModalOpen(false);
        console.log("Review submitted successfully.");
      } else {
        console.error('Failed to submit review:', response.statusText);
      }
    } catch (error) {
      console.error('Error submitting review:', error);
    }
  };

  if (isLoading) {
    return <div className="completed-container loading">Loading...</div>;
  }

  if (error) {
    return <div className="completed-container error">Error: {error.message}</div>;
  }

  return (
    <div className="completed-container">
      <h2>Completed</h2>
      {completedItems.length === 0 ? (
        <div className="no-items-message">No items in the completed list.</div>
      ) : (
        completedItems.map(item => (
          <div key={item._id} className="completed-item">
            <h3>{item.title}</h3>
            <p>Author: {item.author}</p>
            <div className="book-container">
              {item.cover_url ? (
                <img src={item.cover_url} alt="Book Cover" className="book-cover" />
              ) : (
                <div className="no-cover">No Book Cover Available</div>
              )}
              <div>
                <button className="remove-button" onClick={() => handleRemoveFromCompleted(item._id)}>Delete</button>
                <button className="add-review-button" onClick={() => handleModalOpen(item._id, item.bookId, item.author, item.title)}>Add Review</button>
              </div>
            </div>
          </div>
        ))
      )}
      {isModalOpen && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={handleModalClose}>&times;</span>
            <h2>Add Review</h2>
            <form onSubmit={handleSubmit}>
              <div className="rating-container">
                {[...Array(5)].map((_, index) => (
                  <span
                    key={index}
                    onClick={() => setRating(index + 1)}
                    className={index < rating ? 'star-filled' : ''}
                  >
                    {index < rating ? '★' : '☆'}
                  </span>
                ))}
              </div>
              <textarea
                value={review}
                onChange={(event) => setReview(event.target.value)}
                placeholder="Enter your review here..."
                className="review-text"
              />
              <button type="submit" className="submit-review-btn">Submit Review</button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default Completed;
