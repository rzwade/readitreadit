import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from './UserContext';
import './ProfilePage.css';

const ProfilePage = () => {
  const { userInfo, setUserInfo, token } = useContext(UserContext); // Access token from context
  const [loading, setLoading] = useState(true);
  const [userReviews, setUserReviews] = useState([]);
  const [averageRating, setAverageRating] = useState(0);
  const [favoriteBook, setFavoriteBook] = useState('');
  const [favoriteAuthor, setFavoriteAuthor] = useState('');
  const [editingFavorites, setEditingFavorites] = useState(false);

  useEffect(() => {
    const fetchUserProfile = async () => {
      try {
        const response = await fetch('https://readitreadit.onrender.com/profile', {
          credentials: 'include',
          headers: {
            'Authorization': `Bearer ${token}` // Include token in Authorization header
          }
        });
        if (response.ok) {
          const userProfile = await response.json();
          setUserInfo(userProfile); // Update user info in context
          setFavoriteBook(userProfile.favoriteBook || ''); // Set favoriteBook from profile
          setFavoriteAuthor(userProfile.favoriteAuthor || ''); // Set favoriteAuthor from profile
        } else {
          console.error('Error fetching user profile:', response.statusText);
        }
      } catch (error) {
        console.error('Error fetching user profile:', error);
      } finally {
        setLoading(false);
      }
    };

    const fetchUserReviews = async () => {
      if (!userInfo || !userInfo.id) {
        console.error('User ID is not defined.');
        setLoading(false);
        return;
      }

      try {
        const response = await fetch(`https://readitreadit.onrender.com/api/reviews?userId=${userInfo.id}`, {
          credentials: 'include',
          headers: {
            'Authorization': `Bearer ${token}` // Add Authorization header with token
          }
        });
        if (response.ok) {
          const reviews = await response.json();
          setUserReviews(reviews);

          // Calculate the average rating
          if (reviews.length > 0) {
            const totalRating = reviews.reduce((sum, review) => sum + review.rating, 0);
            const avgRating = totalRating / reviews.length;
            setAverageRating(avgRating.toFixed(1)); // Round to one decimal place
          }
        } else {
          console.error('Error fetching user reviews:', response.statusText);
        }
      } catch (error) {
        console.error('Error fetching user reviews:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchUserProfile();
    fetchUserReviews();
  }, [userInfo, setUserInfo, token]);

  const handleUpdateProfile = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('https://readitreadit.onrender.com/profile', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({ favoriteBook, favoriteAuthor }),
        credentials: 'include'
      });
      if (response.ok) {
        const updatedProfile = await response.json();
        setUserInfo(updatedProfile); // Update user info in context
        setEditingFavorites(false); // Exit editing mode
        alert('Profile updated successfully');
      } else {
        console.error('Error updating profile:', response.statusText);
        alert('Failed to update profile');
      }
    } catch (error) {
      console.error('Error updating profile:', error);
      alert('Failed to update profile');
    }
  };

  const renderStars = (rating) => {
    const starStyle = {
      color: '#962715',
    };

    return (
      <p className="star-rating">
        <span style={starStyle}>{'★'.repeat(rating)}</span>
        <span>{'☆'.repeat(5 - rating)}</span>
      </p>
    );
  };

  if (loading) {
    return <div className="profile-container loading">Loading...</div>;
  }

  return (
    <div className="profile-container">
      {userInfo && (
        <div>
          <div className="profile-header">
            <h2>Welcome, {userInfo.username}</h2>
            {/* Display other profile information */}
            <p>Favorite Book: {editingFavorites ? (
              <input
                type="text"
                value={favoriteBook}
                onChange={(e) => setFavoriteBook(e.target.value)}
              />
            ) : (
              userInfo.favoriteBook || 'Not specified'
            )}
            </p>
            <p>Favorite Author: {editingFavorites ? (
              <input
                type="text"
                value={favoriteAuthor}
                onChange={(e) => setFavoriteAuthor(e.target.value)}
              />
            ) : (
              userInfo.favoriteAuthor || 'Not specified'
            )}
            </p>
            {editingFavorites ? (
              <button onClick={handleUpdateProfile}>Save</button>
            ) : (
              <button onClick={() => setEditingFavorites(true)}>Edit Favorites</button>
            )}
          </div>

          <div className="my-reviews">
            <h1>My Reviews</h1>
            <h4>Average Rating Score Given: {averageRating}</h4>
            {userReviews.length === 0 ? (
              <p>No reviews found.</p>
            ) : (
              <ul>
                {userReviews.map(review => (
                  <li key={review._id}>
                    <p>Author: {review.author}</p>
                    <p>Title: {review.title}</p>
                    {renderStars(review.rating)}
                    <p>Review: {review.reviewText}</p>
                  </li>
                ))}
              </ul>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default ProfilePage;
