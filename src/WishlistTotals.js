import React, { useState, useEffect } from 'react';
import './WishlistTotals.css'; // Import CSS file

const WishlistTotals = () => {
  const [wishlistTotals, setWishlistTotals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchWishlistTotals = async () => {
      try {
        const response = await fetch('https://readitreadit.onrender.com/api/wishlistTotals');
        if (response.ok) {
          const data = await response.json();
          setWishlistTotals(data);
        } else {
          setError(new Error('Failed to fetch wishlist totals'));
        }
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchWishlistTotals();
  }, []);

  if (isLoading) {
    return <div className="wishlist-totals-container loading">Loading...</div>;
  }

  if (error) {
    return <div className="wishlist-totals-container error">Error: {error.message}</div>;
  }

  return (
    <div className="wishlist-totals-container">
      <h2>Wishlist Totals</h2>
      <table className="wishlist-table">
        <thead>
          <tr>
            <th>Count</th>
            <th>Title</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
          {wishlistTotals.map(item => (
            <tr key={item._id}>
              <td>{item.count}</td>
              <td>{item._id}</td>
              <td>{item.author}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default WishlistTotals;
