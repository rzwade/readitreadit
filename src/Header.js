import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { UserContext } from './UserContext';
import './Header.css'; // Import the CSS file

const Header = () => {
  const { userInfo, setUserInfo, userId } = useContext(UserContext);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  useEffect(() => {
    const fetchUserProfile = async () => {
      try {
        if (userId) {
          const response = await fetch(`https://readitreadit.onrender.com/profile/${userId}`, {
            credentials: 'include',
          });
          if (response.ok) {
            const userProfile = await response.json();
            setUserInfo(userProfile);
          }
        }
      } catch (error) {
        console.error('Error fetching user profile:', error);
      }
    };
    fetchUserProfile();
  }, [userId, setUserInfo]);

  const logoutAndRedirect = async () => {
    try {
      const response = await fetch('https://readitreadit.onrender.com/logout', {
        method: 'POST',
        credentials: 'include',
      });
      if (response.ok) {
        setUserInfo({});
        // Redirect to home page after successful logout
        window.location.href = '/';
      } else {
        console.error('Error logging out:', response.statusText);
      }
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  const closeMenu = () => {
    setIsMenuOpen(false);
  };

  return (
    <header>
      <Link to="/" onClick={closeMenu}>
        <img src="/images/readit_logo_trans.png" alt="Logo" className="logo" />
      </Link>
      <button className="hamburger" onClick={() => setIsMenuOpen(!isMenuOpen)}>
        ☰
      </button>
      <nav className={isMenuOpen ? 'open' : ''}>
        <ul>
          <li>
            <Link to="/search" onClick={closeMenu}>Search</Link>
          </li>
          <li>
            <Link to="/wishlist" onClick={closeMenu}>Wishlist</Link>
          </li>
          <li>
            <Link to="/completed" className="read-link" onClick={closeMenu}>Read</Link>
          </li>
          <li>
            <Link to="/wishlistTotals" onClick={closeMenu}>Most Wishlisted</Link>
          </li>
          <li>
            <Link to="/completedTotals" className="read-link" onClick={closeMenu}>Most Read</Link>
          </li>
          {userInfo.username && (
            <>
              <li>
                <Link to="/profile" onClick={closeMenu}>Profile</Link>
              </li>
              <li>
                <button className="btn btn-primary display-7" onClick={() => { logoutAndRedirect(); closeMenu(); }}>
                  Log Out
                </button>
              </li>
            </>
          )}
          {!userInfo.username && (
            <>
              <li>
                <Link to="/register" onClick={closeMenu}>Register</Link>
              </li>
              <li>
                <Link to="/login" onClick={closeMenu}>Login</Link>
              </li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
