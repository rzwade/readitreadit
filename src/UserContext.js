import React, { createContext, useState } from "react";

export const UserContext = createContext({});

export function UserContextProvider({ children }) {
  const [userInfo, setUserInfo] = useState({});
  const [accessToken, setAccessToken] = useState(null); // Initialize accessToken as null
  const [userId, setUserId] = useState(null); // Initialize userId as null

  return (
    <UserContext.Provider value={{ userInfo, setUserInfo, accessToken, setAccessToken, userId, setUserId }}>
      {children}
    </UserContext.Provider>
  );
}
