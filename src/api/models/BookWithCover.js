const mongoose = require('mongoose');

const bookWithCoverSchema = new mongoose.Schema({
    key: { type: String, required: true },
    seed: { type: [String], default: [] },
    title: { type: String, required: true },
    first_publish_year: { type: Number, required: true },
    isbn: { type: [String], default: [] },
    author_key: { type: [String], default: [] },
    author_name: { type: [String], required: true },
    subject: { type: [String], default: [] },
    subject_facet: { type: [String], default: [] },
    cover: { type: String, required: true }
  });

  const BookWithCover = mongoose.model('BookWithCover', bookWithCoverSchema);

  module.exports = BookWithCover;
