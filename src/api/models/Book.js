const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
  itemId: { type: String, required: true }, // Unique identifier field
  key: { type: String, required: true },
  seed: { type: [String], default: [] },
  title: { type: String, required: true },
  first_publish_year: { type: Number, required: true },
  isbn: { type: [String], default: [] },
  author_key: { type: [String], default: [] },
  author_name: { type: [String], required: true },
  subject: { type: [String], default: [] },
  subject_facet: { type: [String], default: [] },
  cover: { type: String, default: null }
});

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
