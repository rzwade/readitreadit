const mongoose = require('mongoose');

const WishlistItemSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    bookId: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    cover_url: {
        type: String,
        required: true
    }
});

const WishlistItem = mongoose.model('WishlistItem', WishlistItemSchema);

module.exports = WishlistItem;
