const mongoose = require('mongoose');

const completedItemSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  bookId: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  cover_url: {
    type: String,
    required: true
  },
  // Other fields if any
});

const CompletedItem = mongoose.model('CompletedItem', completedItemSchema);

module.exports = CompletedItem;
