require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const axios = require('axios');
const User = require('./models/User.js');
const WishlistItem = require('./models/Wishlist.js');
const CompletedItem = require('./models/Completed.js');
const wishlistTotalsRouter = require('./wishlistTotals.js');
const completedTotalsRouter = require('./completedTotals.js');
const generateUniqueId = require('./generateUniqueId.js');
const Review = require('./models/Review.js');
const BookQueries = require('./bookQueries.js');

const app = express();

app.use(cors({
  origin: ['http://localhost:3000', 'https://readit-readit.com', 'https://www.readit-readit.com'],
  credentials: true,
}));

app.use(bodyParser.json());
app.use(cookieParser());

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
  console.log('Connected to MongoDB');
});

app.post('/register', async (req, res) => {
  const { username, password } = req.body;
  try {
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      return res.status(400).json({ message: 'Username already exists' });
    }

    const userId = generateUniqueId();

    const newUser = new User({ userId, username, password });
    newUser.password = await bcrypt.hash(password, 10);
    await newUser.save();

    res.status(200).json({ message: 'User registered successfully', id: userId });
  } catch (error) {
    console.error('Error registering user:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/login', async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }

    const token = jwt.sign({ username: user.username, id: user._id }, process.env.JWT_SECRET);

    res.cookie('token', token, { httpOnly: true }).json({ message: 'Login successful', token, id: user._id, username: user.username });
  } catch (error) {
    console.error('Error logging in:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});


app.post('/logout', async (req, res) => {
  try {
    res.clearCookie('token', { httpOnly: true });
    res.status(200).json({ message: 'Logout successful' });
  } catch (error) {
    console.error('Error logging out:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.get('/profile', async (req, res) => {
  try {
    const token = req.cookies.token;
    if (!token) {
      return res.status(401).json({ message: 'Unauthorized' });
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const user = await User.findById(decoded.id);
    if (!user) {
      return res.status(404).json({ message: 'User profile not found' });
    }

    res.status(200).json({
      username: user.username,
      id: user._id,
      favoriteBook: user.favoriteBook, // Include favoriteBook
      favoriteAuthor: user.favoriteAuthor // Include favoriteAuthor
    });
  } catch (error) {
    console.error('Error fetching user profile:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.put('/profile', async (req, res) => {
  const token = req.cookies.token;
  if (!token) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  const { favoriteBook, favoriteAuthor } = req.body;

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const user = await User.findByIdAndUpdate(
      decoded.id,
      { favoriteBook, favoriteAuthor },
      { new: true }
    );
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    res.status(200).json({
      username: user.username,
      id: user._id,
      favoriteBook: user.favoriteBook,
      favoriteAuthor: user.favoriteAuthor
    });
  } catch (error) {
    console.error('Error updating user profile:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.get('/api/books/:query', async (req, res) => {
  const { query } = req.params;
  try {
    const bookQueries = new BookQueries();
    const books = await bookQueries.get_by_query(query);

    const booksWithId = books.map(book => ({ ...book, bookId: generateUniqueId() }));

    res.json(booksWithId);
  } catch (error) {
    console.error('Error searching books:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.get('/api/covers/:isbn', async (req, res) => {
  const { isbn } = req.params;
  try {
    const bookQueries = new BookQueries();
    const coverUrl = await bookQueries.fetch_cover_by_isbn(isbn);

    const coverWithId = { cover_url: coverUrl, coverId: generateUniqueId() };

    res.json(coverWithId);
  } catch (error) {
    console.error('Error fetching book cover:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.get('/api/book-details/:isbn', async (req, res) => {
  const { isbn } = req.params;
  const apiUrl = `https://openlibrary.org/api/volumes/brief/isbn/${isbn}.json`;

  try {
    const response = await axios.get(apiUrl);
    const data = response.data;

    if (data && 'items' in data && data.items.length > 0) {
      const book = data.items[0];
      const bookDetails = {
        title: book.title,
        author_name: book.authors ? book.authors.map(author => author.name) : [],
        first_publish_year: book.publishDate,
        isbn: isbn,
      };

      res.json(bookDetails);
    } else {
      res.status(404).json({ message: 'Book details not found' });
    }
  } catch (error) {
    console.error('Error fetching book details:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/api/wishlist', async (req, res) => {
  try {
    const { userId, bookId, title, author, cover_url } = req.body;

    const newItem = new WishlistItem({ userId, bookId, title, author, cover_url });
    await newItem.save();
    res.status(201).json({ message: 'Wishlist item added successfully' });
  } catch (error) {
    console.error('Error adding wishlist item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.get('/api/wishlist/:userId', async (req, res) => {
  try {
    const { userId } = req.params;
    const items = await WishlistItem.find({ userId });

    const itemsWithId = items.map(item => ({ ...item.toObject(), itemUniqueId: item._id }));

    res.json(itemsWithId);
  } catch (error) {
    console.error('Error fetching wishlist items:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.delete('/api/wishlist/:itemId', async (req, res) => {
  try {
    const { itemId } = req.params;
    const deletedItem = await WishlistItem.findByIdAndDelete(itemId);
    if (!deletedItem) {
      return res.status(404).json({ message: 'Wishlist item not found' });
    }
    res.json({ message: 'Wishlist item removed successfully' });
  } catch (error) {
    console.error('Error removing wishlist item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/api/completed', async (req, res) => {
  try {
    const { userId, bookId, title, author, cover_url } = req.body;

    const newCompletedItem = new CompletedItem({ userId, bookId, title, author, cover_url });
    await newCompletedItem.save();

    res.status(201).json({ message: 'Completed item added successfully' });
  } catch (error) {
    console.error('Error adding completed item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});


app.get('/api/completed/:userId', async (req, res) => {
  try {
    const { userId } = req.params;
    const items = await CompletedItem.find({ userId });

    const itemsWithId = items.map(item => ({ ...item.toObject(), itemUniqueId: item._id }));
    res.json(itemsWithId);
  } catch (error) {
    console.error('Error fetching completed items:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});


app.delete('/api/completed/:itemId', async (req, res) => {
  try {
    const { itemId } = req.params;
    const deletedItem = await CompletedItem.findByIdAndDelete(itemId);
    if (!deletedItem) {
      return res.status(404).json({ message: 'Completed item not found' });
    }
    res.json({ message: 'Completed item removed successfully' });
  } catch (error) {
    console.error('Error removing completed item:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/api/reviews', async (req, res) => {
  try {
    const { reviewText, rating, userId, bookId, author, title } = req.body;

    if (!userId || !bookId || !reviewText) {
      return res.status(400).json({ message: 'userId, bookId, and reviewText are required' });
    }

    const newReview = new Review({
      userId,
      bookId,
      author,
      title,
      rating,
      reviewText,
    });

    const savedReview = await newReview.save();

    res.status(201).json({ message: 'Review added successfully', review: savedReview });
  } catch (error) {
    console.error('Error adding review:', error);
    res.status(500).json({ message: 'Internal server error', error: error.message });
  }
});

app.get('/api/reviews', async (req, res) => {
  try {
    const { userId } = req.query;

    const reviews = await Review.find({ userId });

    res.status(200).json(reviews);
  } catch (error) {
    console.error('Error fetching reviews:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.use('/api/wishlistTotals', wishlistTotalsRouter);
app.use('/api/completedTotals', completedTotalsRouter);

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
