const express = require('express');
const CompletedItem = require('./models/Completed.js');

const router = express.Router();

// Route for fetching accumulated counts of completed items
router.get('/', async (req, res) => {
  try {
    const completedCounts = await CompletedItem.aggregate([
      { $group: { _id: '$title', count: { $sum: 1 }, author: { $first: '$author' } } },
      { $sort: { count: -1 } }
    ]);
    res.json(completedCounts);
  } catch (error) {
    console.error('Error fetching completed totals:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
