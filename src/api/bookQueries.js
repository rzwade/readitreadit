const axios = require('axios'); // Import axios module

class BookQueries {
  async get_by_query(query) {
    const url = `http://openlibrary.org/search.json?q=${query}`;
    try {
      const response = await axios.get(url);
      const data = response.data;

      if (!data || typeof data !== 'object') {
        console.log("Unexpected response format:", data);
        return [];
      }

      const books = data.docs.map(item => ({
        key: item.key,
        title: item.title,
        first_publish_year: item.first_publish_year,
        isbn: item.isbn || [],
        author_name: item.author_name || [],
        cover: null // Initialize cover as null
      }));

      await this.fetch_book_covers(books); // Fetch book covers

      return books;
    } catch (error) {
      console.error("Error fetching data:", error);
      return [];
    }
  }

  async fetch_book_covers(books) {
    for (const book of books) {
      for (const isbn of book.isbn) {
        const coverUrl = await this.fetch_cover_by_isbn(isbn);
        book.cover = coverUrl;
      }
    }
  }

  async fetch_cover_by_isbn(isbn) {
    const coverUrl = `https://covers.openlibrary.org/b/isbn/${isbn}-L.jpg`;
    return coverUrl;
  }
  async get_book_details(isbn) {
    try {
        // Assuming you're using Mongoose to query the database
        const book = await Book.findOne({ isbn: { $in: [isbn] } });

        if (!book) {
            return null; // If book is not found, return null
        }

        // Return the book details
        return {
            title: book.title,
            author: book.author_name.join(', '), // Assuming author_name is an array
            // Add other book details as needed
        };
    } catch (error) {
        throw new Error('Error fetching book details:', error);
    }
}
}

module.exports = BookQueries;
