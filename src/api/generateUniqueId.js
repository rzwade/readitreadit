const crypto = require('crypto');

const generateUniqueId = (bookTitle, author) => {
  const timestamp = Date.now();
  const uniqueString = `${bookTitle}-${author}-${timestamp}`;
  const itemUniqueId = crypto.createHash('md5').update(uniqueString).digest('hex');
  return itemUniqueId;
};

module.exports = generateUniqueId;
