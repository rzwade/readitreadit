const express = require('express');
const WishlistItem = require('./models/Wishlist.js');

const router = express.Router();

// Route for fetching accumulated counts of books added to all wishlists
router.get('/', async (req, res) => {
  try {
    const wishlistCounts = await WishlistItem.aggregate([
      { $group: { _id: '$title', count: { $sum: 1 }, author: { $first: '$author' } } },
      { $sort: { count: -1 } }
    ]);
    res.json(wishlistCounts);
  } catch (error) {
    console.error('Error fetching wishlist totals:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
