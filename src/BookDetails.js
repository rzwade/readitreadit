import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const BookDetails = () => {
  const { ISBN } = useParams();
  const ISBNs = ISBN.split(',');
  const [firstBook, setFirstBook] = useState(null);

  useEffect(() => {
    const fetchBookDetails = async () => {
      try {
        const response = await fetch(`https://readitreadit.onrender.com/api/books/${ISBNs[0]}`);
        const bookData = await response.json();
        const firstBookData = bookData;

        // Fetch cover for the first book only
        const coverResponse = await fetch(`https://covers.openlibrary.org/b/isbn/${ISBNs[0]}-L.jpg`);
        if (coverResponse.ok) {
          const coverBlob = await coverResponse.blob();
          const coverUrl = URL.createObjectURL(coverBlob);

          // Set the first book details with cover URL
          setFirstBook({ ...firstBookData, cover_url: coverUrl });
        } else {
          console.error('Failed to fetch cover image');
          // Set the first book details without cover URL
          setFirstBook(firstBookData);
        }
      } catch (error) {
        console.error('Error fetching book details:', error);
      }
    };

    fetchBookDetails();
  }, [ISBNs]);

  if (!firstBook) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>{firstBook.title}</h2>
      <p>Author: {firstBook.author_name?.join(', ')}</p>
      <p>First Publish Year: {firstBook.first_publish_year}</p>
      <p>ISBN: {firstBook.isbn}</p>
      {firstBook.cover_url && <img src={firstBook.cover_url} alt="Book Cover" />}
    </div>
  );
};

export default BookDetails;
