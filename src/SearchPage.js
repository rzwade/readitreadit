import React, { useState, useEffect, useContext } from 'react';
import { Quotations } from './Quotations'; // Import the Quotations array
import openBookImage from './images/open-book-27.jpg';
import SearchBar from './SearchBar';
import axios from 'axios';
import { UserContext } from './UserContext';
import './SearchPage.css'; // Import CSS file

const SearchPage = () => {
  const { userInfo } = useContext(UserContext); // Destructure userInfo from context
  const [randomQuote, setRandomQuote] = useState('');
  const [results, setResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [wishlist, setWishlist] = useState([]); // State to store wishlist items
  const [completed, setCompleted] = useState([]); // State to store completed items

  // Define fetchWishlist function
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchWishlist = async () => {
    try {
      if (userInfo && userInfo.id) {
        // Fetch the user's wishlist items
        const response = await axios.get(`https://readitreadit.onrender.com/api/wishlist/${userInfo.id}`);
        const wishlistItems = response.data;
        console.log('Fetched wishlist items:', wishlistItems);
        setWishlist(wishlistItems);
      }
    } catch (error) {
      console.error('Error fetching wishlist:', error);
    }
  };

  // Define fetchCompleted function
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchCompleted = async () => {
    try {
      if (userInfo && userInfo.id) {
        // Fetch the user's completed items
        const response = await axios.get(`https://readitreadit.onrender.com/api/completed/${userInfo.id}`);
        const completedItems = response.data;
        console.log('Fetched completed items:', completedItems);
        setCompleted(completedItems);
      }
    } catch (error) {
      console.error('Error fetching completed items:', error);
    }
  };

  useEffect(() => {
    const initialQuote = generateRandomQuote();
    setRandomQuote(initialQuote);
  }, []);

  useEffect(() => {
    console.log('User ID:', userInfo ? userInfo.id : 'Not available');
    console.log('Username:', userInfo ? userInfo.username : 'Not available'); // Log username
  }, [userInfo]); // Include userInfo in dependencies

  useEffect(() => {
    // Call fetchWishlist and fetchCompleted functions
    fetchWishlist();
    fetchCompleted();
  }, [fetchWishlist, fetchCompleted]);
  
  const generateRandomQuote = () => {
    const randomIndex = Math.floor(Math.random() * Quotations.length);
    return Quotations[randomIndex];
  };

  const handleSearch = async (query) => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await axios.get(`https://readitreadit.onrender.com/api/books/${encodeURIComponent(query)}`);
      const data = response.data;
      console.log('Data fetched from search:', data); // Log the fetched data
      const bookResults = await Promise.all(data.map(async (book) => {
        const coverResponse = await axios.get(`https://readitreadit.onrender.com/api/covers/${encodeURIComponent(book.isbn[0])}`);
        const coverData = coverResponse.data;
        const bookWithCover = {
          key: book.key,
          title: book.title,
          first_publish_year: book.first_publish_year,
          isbn: book.isbn,
          author_name: book.author_name,
          cover: coverData.cover_url
        };
        return bookWithCover;
      }));
      console.log('Book results after mapping:', bookResults); // Log the processed book results
      setResults(bookResults);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  const addToWishlist = async (book) => {
    try {
      // Check if the book is already in the wishlist
      const existingBook = wishlist.find(item => item.bookId === book.key);
      if (existingBook) {
        alert('This book is already in your wishlist!');
        return;
      }

      // Make POST request to add the book to the wishlist
      const response = await axios.post('https://readitreadit.onrender.com/api/wishlist', {
        userId: userInfo.id,
        bookId: book.key, // Assuming 'key' is the identifier for the book
        title: book.title,
        author: book.author_name?.join(', '),
        cover_url: book.cover // Adjusted cover to cover_url
      });
      console.log('Added to wishlist:', response.data);
      alert('Book added to wishlist successfully!');
      // Fetch the updated wishlist items after adding the item
      fetchWishlist();
    } catch (error) {
      console.error('Error adding to wishlist:', error);
      // Display a message to the user
      alert(error.message);
    }
  };

  const addToCompleted = async (book) => {
    try {
      // Check if the book is already in the completed list
      const existingBook = completed.find(item => item.bookId === book.key);
      if (existingBook) {
        alert('This book is already in your completed list!');
        return;
      }

      // Make POST request to add the book to the completed list
      const response = await axios.post('https://readitreadit.onrender.com/api/completed', {
        userId: userInfo.id,
        bookId: book.key, // Use the 'key' field as the book ID
        title: book.title,
        author: book.author_name?.join(', '),
        cover_url: book.cover
      });
      console.log('Added to completed:', response.data);
      alert('Book added to completed list successfully!');
      // Fetch the updated completed items after adding the item
      fetchCompleted();
    } catch (error) {
      console.error('Error adding to completed:', error);
      // Display a message to the user
      alert(error.message);
    }
  };

  const handleAddToWishlist = (book) => {
    if (!userInfo || !userInfo.id) {
      console.error('User info or user ID is null');
      return;
    }

    // Call the addToWishlist function to add the book to the wishlist
    addToWishlist(book);
  };

  const handleAddToCompleted = (book) => {
    if (!userInfo || !userInfo.id) {
      console.error('User info or user ID is null');
      return;
    }

    // Call the addToCompleted function to add the book to the completed list
    addToCompleted(book);
  };

  return (
    <div className="home-page">
      {/* <h1 className="main-title">Magic in Books</h1> */}
      <div className="quote-box">
        <h4 className="quote-text">"{randomQuote.text}"</h4>
        <p className="quote-author">- {randomQuote.author}</p>
      </div>
      <img
        src={openBookImage}
        alt="Open Book"
        className="open-book-image"
      />
      <div className="search-bar-container">
        <SearchBar onSearch={handleSearch} />
      </div>
      {isLoading && <div>Loading...</div>}
      {error && <div>Error: {error.message}</div>}
      <div className="results-container">
        {results.length > 0 ? (
          <div>
            <h2>Search Results</h2>
            {results.map((book) => (
              <div key={book.key} className="book-item">
                <h3>{book.title}</h3>
                <p>Author: {book.author_name?.join(', ')}</p>
                <div className="book-container">
                  {book.cover ? (
                    <img src={book.cover} alt="Book Cover" className="book-cover" />
                  ) : (
                    <div className="no-cover">No Book Cover Available</div>
                  )}
                  <div>
                    <button onClick={() => handleAddToWishlist(book)}>Add to Wishlist</button>
                    <button onClick={() => handleAddToCompleted(book)}>Add to Completed</button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div>Enter title, author, or keyword above.</div>
        )}
      </div>
    </div>
  );
};

export default SearchPage;
