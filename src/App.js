import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './Header';
import HomePage from './HomePage';
import Login from './Login';
import Register from './Register';
import ProfilePage from './ProfilePage';
import BookDetails from './BookDetails';
import Wishlist from './Wishlist';
import Completed from './Completed';
import { UserContextProvider } from './UserContext';
import WishlistTotals from './WishlistTotals';
import CompletedTotals from './CompletedTotals';
import SearchPage from './SearchPage';

function App() {
  return (
    <Router>
      <UserContextProvider>
        <Header />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/profile" element={<ProfilePage />} />
          <Route path="/books/:ISBN" element={<BookDetails />} />
          <Route path="/wishlist" element={<Wishlist />} />
          <Route path="/completed" element={<Completed />} />
          <Route path="/wishlistTotals" element={<WishlistTotals />} />
          <Route path="/completedTotals" element={<CompletedTotals />} />
          <Route path="/search" element={<SearchPage />} />
        </Routes>
      </UserContextProvider>
    </Router>
  );
}

export default App;

