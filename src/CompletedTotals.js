import React, { useState, useEffect } from 'react';
import './CompletedTotals.css'; // Import CSS file

const CompletedTotals = () => {
  const [completedTotals, setCompletedTotals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchCompletedTotals = async () => {
      try {
        const response = await fetch('https://readitreadit.onrender.com/api/completedTotals', {
          credentials: 'include',
        });
        if (response.ok) {
          const data = await response.json();
          setCompletedTotals(data);
        } else {
          setError(new Error('Failed to fetch completed totals'));
        }
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchCompletedTotals();
  }, []);

  if (isLoading) {
    return <div className="completed-totals-container loading">Loading...</div>;
  }

  if (error) {
    return <div className="completed-totals-container error">Error: {error.message}</div>;
  }

  return (
    <div className="completed-totals-container">
      <h2>Completed Totals</h2>
      <table className="completed-table">
        <thead>
          <tr>
            <th>Count</th>
            <th>Title</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
          {completedTotals.map(item => (
            <tr key={item._id}>
              <td>{item.count}</td>
              <td>{item._id}</td>
              <td>{item.author}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default CompletedTotals;
