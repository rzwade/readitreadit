import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "./UserContext";
import "./Login.css";

export default function LoginPage() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();
  const { setUserInfo, setAccessToken } = useContext(UserContext); // Use setAccessToken

  async function login(ev) {
    ev.preventDefault();
    try {
      const response = await fetch('https://readitreadit.onrender.com/login', {
        method: 'POST',
        body: JSON.stringify({ username, password }),
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include', // Include credentials for cookies
      });
      if (response.ok) {
        const { token, id, ...userInfo } = await response.json();
        setUserInfo({ id, ...userInfo });
        setAccessToken(token); // Use setAccessToken to save the token
        navigate('/profile');
      } else {
        alert('Wrong credentials');
      }
    } catch (error) {
      console.error('Error logging in:', error);
      alert('Login failed');
    }
  }

  return (
    <form className="login" onSubmit={login}>
      <h1>Login</h1>
      <input
        type="text"
        placeholder="Username"
        value={username}
        onChange={ev => setUsername(ev.target.value)}
        required
      />
      <input
        type="password"
        placeholder="Password"
        value={password}
        onChange={ev => setPassword(ev.target.value)}
        required
      />
      <button type="submit">Login</button>
    </form>
  );
}
