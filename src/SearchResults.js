import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { UserContext } from './UserContext';

const SearchResults = ({ results, isLoading, error, addToWishlist }) => {
  const { userId } = useContext(UserContext); // Access userId from user context

  const handleAddToWishlist = (book) => {
    if (!userId) {
      console.error('User ID is null');
      return;
    }

    addToWishlist(userId, book); // Call the function to add the book to the wishlist
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      <h2>Search Results</h2>
      {results.length === 0 ? (
        <div>No results found. Enter title, author, or keyword above.</div>
      ) : (
        results.map((book) => (
          <div key={book.key}>
            <h3>
              {/* Link to the book details page */}
              <Link to={`/books/${book.isbn}`}>{book.title}</Link>
            </h3>
            <p>Author: {book.author_name?.join(', ')}</p>
            <div className="book-container">
              {book.cover ? (
                <Link to={`/books/${book.isbn}`}>
                  <img src={book.cover} alt="Book Cover" />
                </Link>
              ) : (
                <div>No Book Cover Available</div>
              )}
              <div>
                {/* Add to Wishlist button */}
                <button onClick={() => handleAddToWishlist(book)}>Add to Wishlist</button>
              </div>
            </div>
          </div>
        ))
      )}
    </div>
  );
};

export default SearchResults;
