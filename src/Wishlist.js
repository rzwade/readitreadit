import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from './UserContext';
import './Wishlist.css';

const Wishlist = () => {
  const { userInfo } = useContext(UserContext);
  const [wishlistItems, setWishlistItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchWishlistItems = async () => {
      try {
        if (userInfo?.id) {
          const response = await fetch(`https://readitreadit.onrender.com/api/wishlist/${userInfo.id}`, {
            credentials: 'include',
          });
          if (response.ok) {
            const items = await response.json();
            setWishlistItems(items);
          } else {
            setError(new Error('Failed to fetch wishlist items'));
          }
        }
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchWishlistItems();
  }, [userInfo]);

  const handleRemoveFromWishlist = async (itemId) => {
    try {
      const response = await fetch(`https://readitreadit.onrender.com/api/wishlist/${itemId}`, {
        method: 'DELETE',
        credentials: 'include',
      });
      if (response.ok) {
        setWishlistItems(wishlistItems.filter(item => item._id !== itemId));
      } else {
        setError(new Error('Failed to remove item from wishlist'));
      }
    } catch (error) {
      setError(error);
    }
  };

  const handleMarkAsRead = async (book) => {
    try {
      const response = await fetch(`https://readitreadit.onrender.com/api/completed`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({
          userId: userInfo.id,
          bookId: book.bookId,
          title: book.title,
          author: book.author,
          cover_url: book.cover_url,
        }),
      });
      if (response.ok) {
        handleRemoveFromWishlist(book._id);
      } else {
        setError(new Error('Failed to mark item as read'));
      }
    } catch (error) {
      setError(error);
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div className="wishlist-container">
      <h2 className="wishlist-heading">Wishlist</h2>
      {wishlistItems.length === 0 ? (
        <div className="no-items-message">No items in the wishlist.</div>
      ) : (
        wishlistItems.map(item => (
          <div className="book-item" key={item._id}>
            <h3>{item.title}</h3>
            <p>Author: {item.author}</p>
            <div className="book-container">
              {item.cover_url ? (
                <img src={item.cover_url} alt="Book Cover" />
              ) : (
                <div className="no-cover">No Book Cover Available</div>
              )}
              <div>
                <button onClick={() => handleRemoveFromWishlist(item._id)}>Delete</button>
                <button onClick={() => handleMarkAsRead(item)}>Mark as Read</button>
              </div>
            </div>
          </div>
        ))
      )}
    </div>
  );
};

export default Wishlist;
